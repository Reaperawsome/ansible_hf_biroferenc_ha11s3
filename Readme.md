A házi feladatban a megadott feladatok alapján a controller gép mely elindítja az automatizációt a workstation gépről működik és a servera és a serverb gépekre történik a yml fájlokban megadott automaziált ansible konfiguráció. A RedHat oldalán lévő lab environmenten keresztül használt virtuális gépeket használtam feladat elkészítését követően.

-Az LVM Raid 0 létrehozásánál, mivel 4 lemez került feltételezésre (sda, sdb, sdc, sdd) így az arra megírt yml fájl nem biztos hogy lefog futni, mivel a 4 partició nem volt bekonfigurálva a megadott gépekre (utólag megnéztem és más nevű partíciók léteznek, de mivel feltételezésről van szó így nem volt kiadva feladatként hogy létrekell őket hozni). syntax-check történt meg az elkészített yml fájlal kapcsolatosan.

-A felcsatolás fstab-ba /data/dir -nél az megadott mappa nem létezett a megadott virt. gépeken, így az se futott le viszont a szintaxisa megfelel.

-Bizonyos csomaginstallációk szükségesek voltak a workstation gépen az adott konfigurációk futtatása érdekében (pl. rhel-system-roles installálás stb.)

-A handler.yml fájlban a megadott hálozati profilt indítja újra és kéri le az adott hálozati profilok státuszát.